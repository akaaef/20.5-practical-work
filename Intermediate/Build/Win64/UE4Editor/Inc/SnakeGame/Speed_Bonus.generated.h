// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_Speed_Bonus_generated_h
#error "Speed_Bonus.generated.h already included, missing '#pragma once' in Speed_Bonus.h"
#endif
#define SNAKEGAME_Speed_Bonus_generated_h

#define SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_RPC_WRAPPERS
#define SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpeed_Bonus(); \
	friend struct Z_Construct_UClass_ASpeed_Bonus_Statics; \
public: \
	DECLARE_CLASS(ASpeed_Bonus, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASpeed_Bonus) \
	virtual UObject* _getUObject() const override { return const_cast<ASpeed_Bonus*>(this); }


#define SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_INCLASS \
private: \
	static void StaticRegisterNativesASpeed_Bonus(); \
	friend struct Z_Construct_UClass_ASpeed_Bonus_Statics; \
public: \
	DECLARE_CLASS(ASpeed_Bonus, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASpeed_Bonus) \
	virtual UObject* _getUObject() const override { return const_cast<ASpeed_Bonus*>(this); }


#define SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpeed_Bonus(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpeed_Bonus) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpeed_Bonus); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpeed_Bonus); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpeed_Bonus(ASpeed_Bonus&&); \
	NO_API ASpeed_Bonus(const ASpeed_Bonus&); \
public:


#define SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpeed_Bonus(ASpeed_Bonus&&); \
	NO_API ASpeed_Bonus(const ASpeed_Bonus&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpeed_Bonus); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpeed_Bonus); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpeed_Bonus)


#define SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_Speed_Bonus_h_10_PROLOG
#define SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_INCLASS \
	SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_Speed_Bonus_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ASpeed_Bonus>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_Speed_Bonus_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
