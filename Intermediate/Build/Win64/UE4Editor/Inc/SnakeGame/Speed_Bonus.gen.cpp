// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Speed_Bonus.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpeed_Bonus() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ASpeed_Bonus_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASpeed_Bonus();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ASpeed_Bonus::StaticRegisterNativesASpeed_Bonus()
	{
	}
	UClass* Z_Construct_UClass_ASpeed_Bonus_NoRegister()
	{
		return ASpeed_Bonus::StaticClass();
	}
	struct Z_Construct_UClass_ASpeed_Bonus_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Speed_BonusBP_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Speed_BonusBP;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASpeed_Bonus_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpeed_Bonus_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Speed_Bonus.h" },
		{ "ModuleRelativePath", "Speed_Bonus.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpeed_Bonus_Statics::NewProp_Speed_BonusBP_MetaData[] = {
		{ "Category", "Speed_Bonus" },
		{ "ModuleRelativePath", "Speed_Bonus.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ASpeed_Bonus_Statics::NewProp_Speed_BonusBP = { "Speed_BonusBP", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpeed_Bonus, Speed_BonusBP), Z_Construct_UClass_ASpeed_Bonus_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ASpeed_Bonus_Statics::NewProp_Speed_BonusBP_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpeed_Bonus_Statics::NewProp_Speed_BonusBP_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASpeed_Bonus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpeed_Bonus_Statics::NewProp_Speed_BonusBP,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ASpeed_Bonus_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ASpeed_Bonus, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASpeed_Bonus_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASpeed_Bonus>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASpeed_Bonus_Statics::ClassParams = {
		&ASpeed_Bonus::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ASpeed_Bonus_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ASpeed_Bonus_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASpeed_Bonus_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASpeed_Bonus_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASpeed_Bonus()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASpeed_Bonus_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASpeed_Bonus, 2398273295);
	template<> SNAKEGAME_API UClass* StaticClass<ASpeed_Bonus>()
	{
		return ASpeed_Bonus::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASpeed_Bonus(Z_Construct_UClass_ASpeed_Bonus, &ASpeed_Bonus::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ASpeed_Bonus"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpeed_Bonus);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
