// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"



// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	FirstFood();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();

			int RandomX = FMath::RandRange(-1000,1000);
			int RandomY = FMath::RandRange(-1000,1000);
			this->SetActorLocation(FVector(RandomX, RandomY, 0));
			/*this->Destroy();*/
		}
	}
}

void AFood::FirstFood()
{
	int RandomX = FMath::RandRange(-1000, 1000);
	int RandomY = FMath::RandRange(-1000, 1000);
	FVector Location(RandomX, RandomY, 0);
	FTransform Transform(Location);
	FActorSpawnParameters SpawnParameters = FActorSpawnParameters();
	AFood* actor = GetWorld()->SpawnActor<AFood>(FoodBP,Transform,SpawnParameters);
}
