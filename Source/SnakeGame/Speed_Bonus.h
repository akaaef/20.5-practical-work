// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Speed_Bonus.generated.h"

UCLASS()
class SNAKEGAME_API ASpeed_Bonus : public AActor,public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpeed_Bonus();
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<ASpeed_Bonus>Speed_BonusBP;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Interact(AActor* Interactor, bool bIsHead);
	void FirstSpeedBonus();
};
