// Fill out your copyright notice in the Description page of Project Settings.


#include "Speed_Bonus.h"
#include "SnakeBase.h"

// Sets default values
ASpeed_Bonus::ASpeed_Bonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeed_Bonus::BeginPlay()
{
	Super::BeginPlay();
	FirstSpeedBonus();
	
}

// Called every frame
void ASpeed_Bonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeed_Bonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SpeedBonus();
			this->Destroy();
		}
	}
}

void ASpeed_Bonus::FirstSpeedBonus()
{
	for (int i = 0; i < 4; i++)
	{
		int RandomX = FMath::RandRange(-1000, 1000);
		int RandomY = FMath::RandRange(-1000, 1000);
		FVector Location(RandomX, RandomY, 0);
		FTransform Transform(Location);
		FActorSpawnParameters SpawnParameters = FActorSpawnParameters();
		ASpeed_Bonus* actor = GetWorld()->SpawnActor<ASpeed_Bonus>(Speed_BonusBP, Transform, SpawnParameters);
	}
}